<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.master');
// });

Auth::routes();

Route::get('/comments', function () {
    return view('comments.create');
});

Route::resource('posts', 'PostController');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/profile/create', 'ProfileController@create')->name('profile.create');
Route::post('/profile', 'ProfileController@store')->name('profile.store');
Route::get('/profile', 'ProfileController@index')->name('profile.index');
Route::get('/profile/{id}', 'ProfileController@show')->name('profile.show');
Route::get('/profile/{id}/edit', 'ProfileController@edit')->name('profile.edit');
Route::put('/profile/{id}', 'ProfileController@update')->name('profile.update');
Route::post('/profollow/{id}', 'ProfileController@follow')->name('profile.follow');

Route::resource('comments', 'CommentsController');
