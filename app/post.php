<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\post;
use App\comment;

class post extends Model
{
    protected $guarded = [];
    //protected $table = "posts";
 
    public function comment()
    {
        return $this->belongsToMany('App\comment','comments_id', 'id');
    }
}