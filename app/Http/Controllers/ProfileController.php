<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\profile;
use App\users_follow_users;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        return view('profile.create');
    }

    public function store(Request $request){
        // dd($request);

        $request->validate([
            'name' => 'required|max:255',
            'phone' => 'required|max:255',
            'status' => 'required|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images'), $imageName);
  
        $profile = profile::create([
            "name" => $request["name"],
            "phone" => $request["phone"],
            "status" => $request["status"],
            "image" => $imageName,
            'users_id' => Auth::id()
        ]);

        // return back()
        //     ->with('success','You have successfully upload image.')
        //     ->with('image',$imageName);
        return redirect('/profile/'.Auth::user()->profile->id)->with('success','You have successfully completed your profile!');
        // return redirect()->action([ProfileController::class, 'index'])->with('success','You have successfully completed your profile.');

    }

    public function index() {
        $profile = profile::all()->except(Auth::user()->profile->id);
        return view('profile.index', compact('profile'));
    }

    public function show($id) {
        $profile = profile::find($id);
        $viewed_user = User::find($profile->user->id)->id;
        $active_user = Auth::id();

        // echo $active_user;
        // echo $viewed_user;

        $following_count = User::find($viewed_user)->follow()->where('users_id', $viewed_user)->get()->count();
        $followers_count = User::find($viewed_user)->follow()->where('following_id', $viewed_user)->get()->count();
        $follow_status = User::find($active_user)
                            ->follow()
                            ->where('users_id', $active_user)
                            ->where('following_id', $viewed_user)
                            ->get()->count();

        return view('profile.show', compact('profile', 'following_count', 'followers_count', 'follow_status'));
    }

    public function edit($id) {
        $profile = profile::firstwhere('id', $id);
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request) {
        // dd($request);
        $request->validate([
            'name' => 'max:255',
            'phone' => 'max:255',
            'status' => 'max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if($files = $request->file('image')) {
            $imageName = time().'.'.$files->extension();  
            $request->image->move(public_path('images'), $imageName);
        } else {
            $imageName = Auth::user()->profile->image;
        }
        
        // dd($imageName);
       
        $profile = profile::where('id', $id)->update([
            "name" => $request["name"],
            "phone" => $request["phone"],
            "status" => $request["status"],
            "image" => $imageName
        ]);

        return redirect('/profile/'.Auth::user()->profile->id)->with('success', 'You have succesfully updated your profile!');
    }

    public function follow($id, Request $request){

        $user_id = profile::find($id)->user()->first()->id;

        $profile = users_follow_users::create([
            "users_id" => Auth::id(),
            "following_id" => $user_id,
        ]);

        return redirect('/profile')->with('success', 'You have just followed a user!');
    }
}
