<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\post;

class PostController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = post::all(); //pake Eloquent
        return view('posts.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     "quote" => 'required|max:255',
        //     "story" => 'required|max:255',
        //     "picture_url" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     "picture_caption" => 'required|max:255'
        // ]);

        $imageName = time() . '.' . $request->image->extension();
        $request->image->move(public_path('pictures'), $imageName);

        $post = post::create([
            "quote" => $request["quote"],
            "story" => $request["story"],
            "picture_url" => $imageName,
            "picture_caption" => $request["picture_caption"]
        ]);

        return redirect('/posts')->with('success', 'Your data has been saved..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = post::find($id); //pake Eloquent
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = post::find($id); //pake Eloquent
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'quote' => 'max:255',
            'story' => 'max:255',
            'picture_url' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'picture_caption' => 'max:255'
        ]);

        $post = post::find($id);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('pictures/', $filename);
            $post->image = $filename;
        }
        // $post->save();
        $post = post::where('id', $id)->update([
            'quote' => $request['quote'],
            'story' => $request['story'],
            "picture_url" => $filename,
            "picture_caption" => $request["picture_caption"]
        ]); //pake Eloquent

        return redirect('/posts')->with('success', 'Your data has been edited..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        post::destroy($id); //pake Eloquent
        return redirect('/posts')->with('success', 'Your data has been deleted..');
    }
}
