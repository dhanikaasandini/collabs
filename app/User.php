<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile() {
        return $this->hasOne('App\profile', 'users_id', 'id');
    }
    
    public function follow() {
        return $this->belongsToMany('App\User', 'users_follow_users', 'users_id', 'following_id');
    }

    // public function like_posts() {
    //     return $this->belongsToMany('App\post', 'users_like_posts', 'users_id', 'posts_id');
    // }

    // public function like_comments() {
    //     return $this->belongsToMany('App\comment', 'users_like_comments', 'users_id', 'comments_id');
    // }

    public function comment() {
        return $this->belongsToMany('App\comment', 'users_id', 'comments_id');
    }
}
