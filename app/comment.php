<?php

namespace App;
use DB;
use App\comment;


use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table= "comments";
    protected $fillable = ["body"];

    public function comment(){
    return $this->belongsToMany('App\User', 'comments_id');
    }
}
