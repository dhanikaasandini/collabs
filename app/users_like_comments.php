<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users_like_comments extends Model
{
    
    protected $guarded = [];
    public function user()
    {
     return $this->belongsToMany('App\User');
    }  
    public function comment()
    {
     return $this->belongsToMany('App\comment');
    }
  }

