@extends('layouts.master')

@section('content')
<!-- <div class="mt75 row justify-content-center">
    <div class="col-12">
        <textarea placeholder="Write Your Comments Here..." class="form-control" cols="2" rows="2"></textarea>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-primary">Send</button>
    </div>
</div> -->
<a href="/posts"><img src="/assets/img/left-arrow.png" style="width:12px; height:12px;" alt="back"></a>
<div class="ml-5 mt-5 mr-5">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create a Post</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/posts" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="quote"><b>Quote</b></label>
                    <input type="text" class="form-control" id="quote" name="quote" placeholder="Write Your Quote...">

                </div>
                <div class="form-group">
                    <label for="story"><b>Story</b></label>
                    <textarea class="form-control" rows="3" id="story" name="story" placeholder="Write Your Story..." style="margin-top: 0px; margin-bottom: 0px; height: 124px;"></textarea>
                </div>
                <div class="form-group">
                    <b>File Picture</b><br />
                    <input id="image" type="file" class="form-control" name="image">
                </div>

                <div class="form-group">
                    <b>Picture caption</b>
                    <textarea class="form-control" id="picture_caption" name="picture_caption"></textarea>
                </div>

                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Post</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection