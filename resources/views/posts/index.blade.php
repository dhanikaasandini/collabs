@extends('layouts.master')

@section('content')
<div class="mt-3 ml-5 mr-2">
      <div class="card">
            <div class="card-header">
                  <h3 class="card-title">Posts</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                  @if (session('success'))
                  <div class="alert alert-success">
                        {{session('success')}}
                  </div>
                  @endif
                  <a class="btn btn-outline-success mb-2" href="/posts/create">Create New Post</a>
                  <!-- untuk href-nya bisa pake cara laravel route name sbb-> href="{{ route('posts.create') }}" -->
                  <table class="table table-bordered">
                        <thead>
                              <tr>
                                    <th>Quote</th>
                                    <th>Story</th>
                                    <th>Picture URL</th>
                                    <th>Caption</th>
                                    <th style="width: 40px"><i>Actions</i></th>
                              </tr>
                        </thead>
                        <tbody>
                              @forelse($post as $key => $post)
                              <tr>
                                    <td>
                                          {{ $post->quote }}
                                    </td>
                                    <td>
                                          {{ $post->story }}
                                    </td>
                                    <td>
                                    <img src="/pictures/{{ $post->picture_url }}" width="50%px">
                                    </td>
                                    <td>
                                          {{ $post->picture_caption }}
                                    </td>
                                    <td style="display:flex">
                                          <a href="/posts/{{$post->id}}" class="btn btn-info btn-sm">show</a>

                                          <a href="/posts/{{$post->id}}/edit" class="btn btn-default btn-sm">edit</a>

                                          <a href="/comments" class="btn btn-default btn-sm">view comment</a>

                                          <form action="/posts/{{$post->id}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <input type="submit" class="btn btn-danger btn-sm" value="del">
                                          </form>

                                    </td>
                              </tr>
                              @empty
                              <tr>
                                    <td colspan="4" align="center" class="text-primary">
                                          No Post(s)
                                    </td>
                              </tr>
                              @endforelse

                        </tbody>
                  </table>
            </div>
            <!-- /.card-body -->

      </div>
</div>
@endsection