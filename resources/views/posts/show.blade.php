@extends('layouts.master')


@section('content')
<h5><a href="/posts"><img src="/assets/img/left-arrow.png" style="width:12px; height:12px;" alt="back"></a> Show</h5>

<div class="mt-5 ml-3">
      <p>quote:</p>
      <h3>{{$post->quote}}</h3>
      <p>story:</p>
      <h5>{{$post->story}}</h5>
      <br>
      <p> Display Image :</p>
      <img src="/pictures/{{ $post->picture_url }}" width="50%px">
      <br>
      <p> caption:</p>
      <h4>{{$post->picture_caption}}</h4>
</div>

@endsection