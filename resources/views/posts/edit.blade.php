@extends('layouts.master')

@section('content')
<h5><a href="/posts"><img src="/assets/img/left-arrow.png" style="width:12px; height:12px;" alt="back"></a></h5>
<div class="ml-5 mt-5 mr-5">
      <div class="card card-primary">
            <div class="card-header">
                  <h3 class="card-title">Edit Post</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/posts/{{$post->id}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="card-body">
                        <div class="form-group">
                              <label for="quote">Quote</label>
                              <input type="text" class="form-control" id="quote" name="quote" value="{{ old('quote', $post->quote) }}" placeholder="masukan quote ...">
                              @error('quote')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                        </div>
                        <div class="form-group">
                              <label for="story">Story</label>
                              <textarea class="form-control" rows="3" id="story" name="story" style="margin-top: 0px; margin-bottom: 0px; height: 124px;">{{ old('story', $post->story) }}</textarea>
                              @error('story')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                        </div>
                        <div class="form-group">
                              <b>File Picture</b><br />
                              <img src="/pictures/{{ $post->picture_url }}" width="30%">
                              File Name : {{ $post->picture_url }}
                              <input id="image" type="file" class="form-control" name="image">
                              @error('image')
                              <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                        </div>
                        <div class="form-group">
                              <label for="story">Caption</label>
                              <textarea class="form-control" rows="3" id="picture_caption" name="picture_caption" style="margin-top: 0px; margin-bottom: 0px; height: 124px;">{{ old('picture_caption', $post->picture_caption) }}</textarea>
                              @error('picture_caption')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                        </div>

                  </div>

                  <div class="card-footer d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
            </form>
      </div>
</div>
@endsection