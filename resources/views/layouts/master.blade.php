<!DOCTYPE html>
<html lang="en">

<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta name="description" content="Cocoon -Portfolio">
    <meta name="keywords" content="Cocoon , Portfolio">
    <meta name="author" content="Pharaohlab">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ========== Title ========== -->
    <title> Final Project </title>
    <!-- ========== Favicon Ico ========== -->
    <!--<link rel="icon" href="fav.ico">-->
    <!-- ========== STYLESHEETS ========== -->
    <!-- Bootstrap CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts Icon CSS -->
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/et-line.css" rel="stylesheet">
    <link href="/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Carousel CSS -->
    <link href="/assets/css/slick.css" rel="stylesheet">
    <!-- Magnific-popup -->
    <link rel="stylesheet" href="/assets/css/magnific-popup.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="/assets/css/animate.min.css">
    <!-- Custom styles for this template -->
    <link href="/assets/css/main.css" rel="stylesheet">
</head>

<body>
    <div class="loader">
        <div class="loader-outter"></div>
        <div class="loader-inner"></div>
    </div>

    <div class="body-container container-fluid">
        <a class="menu-btn" href="javascript:void(0)">
            <i class="ion ion-grid"></i>
        </a>
        <div class="row justify-content-center">
            <!--=================== side menu ====================-->
            <div class="col-lg-2 col-md-3 col-12 menu_block">
                
                <!--logo -->
                <div class="logo_box">
                    <a href="#">
                        <img src="/assets/img/logo.png" alt="cocoon">
                    </a>
                </div>
                <!--logo end-->
                @guest
                    <div class="logo_box py-1 mx-auto" style="width: 80px">
                        <div class="text-center">
                            <img src="{{ asset('/assets/img/ava.png')}}" alt="Profile Image">
                            <p style="color: white">Guest</p>
                        </div>
                    </div>
                    <!--main menu -->
                    <div class="side_menu_section mt-2">                  
                        <ul class="menu_nav">
                            <li>
                                <a class="nav-link dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link dropdown-item" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <!--main menu end -->

                @else
                    <div class="logo_box py-1 mx-auto" style="width: 80px">
                        <div class="text-center">
                            @if (Auth::user()->profile != NULL)
                                <img src="/images/{{ Auth::user()->profile->image }}" alt="Profile Image">
                            @else
                                <img src="{{ asset('/assets/img/ava.png')}}" alt="Profile Image">
                            @endif
                            <p style="color: white">{{ Auth::user()->username }}</p>
                        </div>
                    </div>
                    <!--main menu -->
                    <div class="side_menu_section mt-2">                  
                        <ul class="menu_nav">
                            @if (Auth::user()->profile != NULL)
                                <li>
                                    <a class="dropdown-item" href="{{ url('profile/'.Auth::user()->profile->id) }}">View Profile</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ url('profile/'.Auth::user()->profile->id).'/edit' }}">Edit Profile</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ url('profile/') }}">Browse Users</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ url('posts/') }}">Posts</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
        
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endif
                            
                        </ul>
                    </div>
                    <!--main menu end -->
                @endguest

                <!-- copyright -->
                <div class="side_menu_bottom">
                    <div class="side_menu_bottom_inner">
                        <div class="copy_right">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            <p class="copyright">Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                    </div>
                </div>
                <!-- copyright end -->
            </div>
            <!--=================== side menu end====================-->

            <!--=================== content body ====================-->
            <div class="col-lg-10 col-md-9 col-12 body_block  align-content-center">


                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
                <!-- /.content -->

            </div>
            <!--=================== content body end ====================-->
        </div>
    </div>


    <!-- jquery -->
    <script src="/assets/js/jquery.min.js"></script>
    <!-- bootstrap -->
    <script src="/assets/js/popper.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/waypoints.min.js"></script>
    <!--slick carousel -->
    <script src="/assets/js/slick.min.js"></script>
    <!--Portfolio Filter-->
    <script src="/assets/js/imgloaded.js"></script>
    <script src="/assets/js/isotope.js"></script>
    <!-- Magnific-popup -->
    <script src="/assets/js/jquery.magnific-popup.min.js"></script>
    <!--Counter-->
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <!-- WOW JS -->
    <script src="/assets/js/wow.min.js"></script>
    <!-- Custom js -->
    <script src="/assets/js/main.js"></script>
</body>

</html>