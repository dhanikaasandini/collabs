@extends('layouts.master')

@section('content')

<div class="testimonial_carousel mt50 slick-initialized slick-slider slick-dotted">

                                        <div class="slick-track" ;">
                                        <div class="testimonial_box slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 373px;">
                                        </div>
                                            <div class="testimonial_box slick-slide" data-slick-index="1" aria-hidden="true" tabindex="-1" role="tabpanel" id="slick-slide01" aria-describedby="slick-slide-control01" style="width: 373px;">
                                            <h4>{{ $comment->body }}</h4>
                                            
                                            <p>Created at: {{$comment->created_at}}</p>
                                            <p>Updated at: {{$comment->updated_at}}</p>
                                            

                                            <div class="testimonial_author">
                    
                                                <img src="/assets/img/user.png" alt="author">
                                                <h5>maria smith</h5>
                                                <p>project manager <span>company</span></p>
                                            </div>
                                        </div></div></div>

                                        

                                        

                                        
                                    


@endsection