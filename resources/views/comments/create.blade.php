@extends('layouts.master')

@section('content')
<form role="form" action="{{route('comments.index')}}" method="POST"> 
@csrf
    <div class="mt75 row justify-content-center">
                                            <div class="col-12">
                                                <input type="textarea" placeholder="Write Your Comments Here.." class="form-control" cols="3" rows="3" id="body" name="body" value="{{old('body', '' )}}" ></textarea>
                                                @error('body')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">Comment</button>
                                            </div>



</div>
@endsection