@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in!

                    {{-- @if (Auth::user()->profile == NULL)
                        <p>Before continue, you need to complete your profile</p>
                        <a class="btn btn-primary" href="{{ route('profile.create')}}">Complete Profile</a>              
                    @endif --}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
