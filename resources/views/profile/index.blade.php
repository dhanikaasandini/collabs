@extends('layouts.master')

@section('content')

<h3 class="card-title">Browse Users</h3>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          
          @if (session('success'))
              <div class="alert alert-success" role="alert">
                  {{ session('success') }}
              </div>
          @endif
          
          <div class="content_box">
            <div class="content_box_inner">
                <div class="row justify-content-center">

                  @forelse ($profile as $key => $item)   
                    <div class="col-md-3 col-12 mx-2 my-2" style="border: 1px solid black">
                      <div class="img_box_one text-left text-center">
                          <img src="/images/{{ $item->image }}" alt="Profile Image">
                          <div class="content">
                              <h5> {{ $item->name }} </h5>
                              <p>  
                                  @if ((strlen($item->status)) > 20)
                                    <p> {{ substr($item->status, 0, 20) }} ...</p>
                                  @else
                                    <p> {{ $item->status }} </p>
                                  @endif
                                  
                              </p>
                              <a href="{{ url('profile/'.$item->id) }}" class="btn btn-info btn-sm">View Profile</a>
                          </div>
                      </div>
                    </div>
                
                  @empty
                      No Other Users
                  @endforelse
                    

                </div>
            </div>
        </div>
              
        </div>
    </div>
</div>

@endsection