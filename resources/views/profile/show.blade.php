@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">Profile</div>

                <div class="card-body">
                    <p> Display Image :
                        <img src="/images/{{ $profile->image }}" width="70px">
                    </p>
                    <p class="card-text">Name : {{ $profile->name }}</p>
                    <p class="card-text">Phone : {{ $profile->phone }}</p>
                    <p class="card-text">Status : {{ $profile->status }}</p>
                    <p class="card-text">Followers : {{ $followers_count }} </p>
                    <p class="card-text">Following : {{ $following_count }} </p>

                    @if ($profile->id != Auth::user()->id)
                        @if ($follow_status == 0)
                            <hr>
                            <form action="{{ url('profile/follow/'.$profile->id) }}" method="POST">
                                @csrf
                                <input type="submit" value="Follow" class="btn btn-primary">
                            </form>
                        @else
                            <hr>
                            <p>You are already following this user.</p>
                        @endif
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection